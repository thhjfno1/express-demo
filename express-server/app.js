/*
 * @Description: 
 * @Author: 
 * @Date: 2021-07-29 12:00:15
 * @LastEditTime: 2021-09-09 16:30:20
 * @LastEditors: Tuohaohu
 * @Usage: 
 */
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var querystring = require("querystring");
const url = require('url');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tecentRouter = require('./routes/tecent');
var videoRouter = require('./routes/video');

var app = express();

// view engine setup
//1.set方法用于指定变量的值
//__dirname 总是指向被执行 js 文件的绝对路径
app.set('views', path.join(__dirname, 'views'));

//app.set('view engine', 'jade'); //默认 他的语法借鉴了Haml
app.set('view engine', 'html');
app.engine( '.html', require( 'ejs' ).__express ); //让ejs能够识别后缀为’.html’的文件 或者设置文件后缀


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/tecent', tecentRouter);
app.use('/video', videoRouter);
app.use('/use',function(req, res, next) {
  //query方式传参
  var urlObj = url.parse(req.url);
  var query = urlObj.query;
  var queryObj = querystring.parse(query);

  res.send('respond with a use');
});

//动态路由
app.use('/userinfo/:id',function(req, res, next) {
  //query方式传参
  //req.params获取动态路由的传值
  var aid = req.params.id; 
   //res.json({res:"respond with a use " +aid});
  res.send({res:"respond with a use " +aid});
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  //res.send('respond with a 404 --ttt');
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error'); //将会根据views中的模板文件进行渲染
});
// process.env.PORT =8888
module.exports = app;
