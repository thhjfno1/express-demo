/*
 * @Description: 
 * @Author: 
 * @Date: 2021-07-29 14:10:09
 * @LastEditTime: 2021-07-29 15:43:11
 * @LastEditors: Tuohaohu
 * @Usage: 
 */
var express = require('express');
var router = express.Router();

const projectCtl = require('./../controllers/project')
/* GET home page. */
router.post('/CreateProject', projectCtl.createProject);

module.exports = router;
