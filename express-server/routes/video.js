/*
 * @Description: 
 * @Author: 
 * @Date: 2021-08-05 15:32:29
 * @LastEditTime: 2021-09-09 14:29:09
 * @LastEditors: Tuohaohu
 * @Usage: 
 */
var express = require('express');
var router = express.Router();
const fs = require('fs')
var path= require('path')
var https = require('https')
var request = require('request');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
// router.get('/blobUrl', function(req, res, next) {
//   https.get('https://playback.eeo.cn/794b4a11vodbj1252412222/3cc0dc3b3701925922763420035/f0.mp4', function (response) {
//     response.setEncoding('binary');  //二进制binary
//     var Data
//     response.on('data', function (data) {    //加载到内存
//       Data += data;
//   }).on('end', function () {          //加载完
//       res.writeHead(200, { 'Access-Control-Allow-Origin': '*', "Content-Type":  response.headers["content-type"] });   //设置头，允许跨域
//       res.write(Data , "binary");
//       res.end();
//    })
//   })
// });
const video = async (req,res, next) => {
  try {
    // open 一个放在服务器的视频
    var paths=path.resolve(__dirname,'../public/video/oceans.mp4')
    let data = fs.readFileSync(paths)
      //res.writeHead(200, { 'Access-Control-Allow-Origin': '*', "Content-Type":  "video/mp4" });   //设置头，允许跨域
     // res.header("Access-Control-Allow-Origin", "*")
      // res.send(data)
      res.writeHead(200, { 'Access-Control-Allow-Origin': '*'});   //设置头，允许跨
      res.write(data , "binary");
      res.end();
  } catch (e) {
    return Promise.reject({
      status: 500,
      message: '视频传输错误'
    })
  }
}

router.get('/blobUrl', video)
router.post('/CreateProject', function(req, res, next) {
    request.post('http://127.0.32.157:4000/users/name', {}, function(err,  data) {
    res.send(data.body);
      //console.log('返回: ' + body);
      return;
  })
  // res.send('respond with a resource');
});

module.exports = router;