/*
 * @Description: 
 * @Author: 
 * @Date: 2021-07-29 14:38:18
 * @LastEditTime: 2021-07-29 16:41:06
 * @LastEditors: Tuohaohu
 * @Usage: 
 */
const tencentcloud = require("tencentcloud-sdk-nodejs")
const utils = require("./../utils/index");
const errcode = require("./../utils/errcode");
const config = require("../config");
const sign = require("./../utils/sign");

// 导入对应产品模块的client models。
const CmeClient = tencentcloud.cme.v20191029.Client
const CmeModel = tencentcloud.cme.v20191029.Models;
const Credential = tencentcloud.common.Credential;
const PersonType = "PERSON";

const cred = new Credential(config.secretId, config.secretKey);
const client = new CmeClient(cred, "ap-guangzhou");

// const clientConfig = {
// // 腾讯云认证信息
// credential: {
//   secretId: "AKIDgCj5FCqDdrsgNvPdmjaOr9MVEg5M7KzV",
//   secretKey: "i6IOq66bMuxqxuhIjvvoSN5e3f9ekmfL",
// },
// // 产品地域
// region: "ap-shanghai",
// // 可选配置实例
// profile: {
//   signMethod: "HmacSHA256", // 签名方法
//   httpProfile: {
//     reqMethod: "POST", // 请求方法
//     reqTimeout: 30, // 请求超时时间，默认60s
//   },
// },
// }
// // 实例化要请求产品(以cvm为例)的client对象
// const client = new CvmClient(clientConfig)


function doCreateProject(ctx, createProjectReq) {
  const params =JSON.parse(ctx.body.data) ;
  return new Promise((resolve, reject) => {
    client.CreateProject(createProjectReq, function (err, response) {
      if (err) {
        console.error(err);
        reject(errcode.systemErr);
      } else {
        console.debug("response", response);
        let resp = utils.buildResp({
          ProjectId: response.ProjectId,
          RequestId: response.RequestId,
          Signature: sign.getOpenProjectSign(
            response.ProjectId,
            params.Owner.Id
          ),
        });
        console.debug("resp", resp);
        resolve(resp);
      }
    });
  });
}

module.exports = {
  //CreateProject官网API链接[]
  async createProject(ctx) {
    const params =JSON.parse(ctx.body.data) ;
    console.log('---------------',params);
    let createProjectReq = new CmeModel.CreateProjectRequest();
    createProjectReq.Name = params.Name;
    createProjectReq.Category = params.Category;
    createProjectReq.AspectRatio = params.AspectRatio;
    createProjectReq.Platform = 'cmetrial';
    let owner = new CmeModel.Entity();
    owner.Id = params.Owner.Id;
    owner.Type = PersonType; //demo仅演示创建PERSON类型的项目，开发者可以按照自身业务需求选择PERSON/TEAM类型的项目
    createProjectReq.Owner = owner;
    console.debug(createProjectReq.to_json_string());
    let resp = await doCreateProject(ctx, createProjectReq);
    ctx.body = resp;
    console.debug("resp await", resp);
  }
}