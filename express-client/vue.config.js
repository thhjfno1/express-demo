/*
 * @Description: 
 * @Author: 
 * @Date: 2021-07-28 16:05:10
 * @LastEditTime: 2021-08-05 16:30:50
 * @LastEditors: Tuohaohu
 * @Usage: 
 */

// vue.config.js
module.exports = {
  devServer: {
    proxy: {
      '/tecent': {
        target: 'http://10.0.32.157:3000',
        changeOrigin: true,
        logLevel:'debug',
      },
      '/video': {
        target: 'http://10.0.32.157:3000',
        changeOrigin: true,
        logLevel:'debug',
      }
    },
    // 此处开启 https
    // https: true
  }
}