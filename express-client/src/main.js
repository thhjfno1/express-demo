/*
 * @Description: 
 * @Author: 
 * @Date: 2021-07-26 16:44:51
 * @LastEditTime: 2021-07-29 15:00:15
 * @LastEditors: Tuohaohu
 * @Usage: 
 */
import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import  qs from 'qs'



Vue.prototype.$qs = qs

Vue.prototype.$axios = axios    //全局注册，使用方法为:this.$axios
Vue.config.productionTip = false
new Vue({
  render: h => h(App),
}).$mount('#app')
